package exercise.chapter16;

import java.util.LinkedList;
import java.util.Scanner;

public class SortedLinkedList {

    private static LinkedList<Integer> list = new LinkedList<>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number;
        try {
            while (true) {
                System.out.println("Please inter your number:");
                number = Integer.parseInt(scanner.nextLine());
                addToList(number);
                showList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static void addToList(int item) {
        if (list.isEmpty()) {
            list.add(item);
        } else if (list.getFirst() > item) {
            list.addFirst(item);
        } else if (list.getLast() < item) {
            list.addLast(item);
        } else {
            boolean go = true;
            int i = 0;
            while (go) {
                if (list.get(i) < item) {
                    i++;
                } else {
                    list.add(i, item);
                    go = false;
                }
            }
        }
    }


    private static void showList() {
        for (Integer num : list) {
            System.out.println(num);
        }
    }

}
