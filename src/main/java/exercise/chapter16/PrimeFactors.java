package exercise.chapter16;

import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class PrimeFactors {

    private static Set<Integer> factorList = new TreeSet<>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter your number:");
        int number = Integer.parseInt(scanner.next());

        findPrimeFactors(number);

        if (factorList.size() == 1) {
            System.out.println(number + " is prime.");
        } else {
            System.out.println(number + " is not prime.");
            System.out.println("Prime factors of this number is:");
            for (Integer integer : factorList) {
                System.out.println(integer);
            }
        }
    }


    private static void findPrimeFactors(int number) {
        if (number <= 1) {
            return;
        } else {
            int i = 2;
            boolean isPrime = true;
            while (i <= number && isPrime) {
                if (number % i == 0) {
                    factorList.add(i);
                    isPrime = false;
                    findPrimeFactors(number / i);
                } else {
                    i++;
                }
            }
        }
    }
}

