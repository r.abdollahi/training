package exercise.chapter16;

import java.util.*;

public class SplitToLetter {

    public static void main(String[] args) {
        Map<String, Integer> myMap = new HashMap<String, Integer>();
        createMap(myMap);
        displayMap(myMap);
    }


    private static void createMap(Map<String, Integer> map) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter a string:");
        String input = scanner.nextLine();
        String[] tokens = input.split("");

        for (String letter : tokens) {
//            if (map.containsKey(letter)) {
//                int count = map.get(letter);
//                map.put(letter, count + 1);
//            } else {
//                map.put(letter, 1);
//            }
            // Your solution is correct too. But this is more succint:
            Integer count = map.getOrDefault(letter, 0);
            map.put(letter, count + 1);
        }
    }


    private static void displayMap(Map<String, Integer> map) {
        TreeSet<String> sortedKeys = new TreeSet<>(map.keySet());
        for (String key : sortedKeys) {
            System.out.println(key + " count is " + map.get(key));
        }
    }
}
