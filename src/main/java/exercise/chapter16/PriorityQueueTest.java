package exercise.chapter16;

import java.util.PriorityQueue;
import java.util.Scanner;

public class PriorityQueueTest {

    public static void main(String[] args) {
        PriorityQueue<Double> queue = new PriorityQueue<>(new CustomComparator());
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter 5 float number:");

        try {
            for (int counter = 0; counter < 5; counter++) {
                String input = scanner.next();
                queue.add(Double.parseDouble(input));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Polling from queue: ");
        while (!queue.isEmpty()) {
            System.out.println(queue.peek());
            queue.poll();
        }

    }
}
