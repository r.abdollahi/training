package exercise.chapter16;

import java.util.LinkedList;
import java.util.Scanner;

public class ReverseLinkedList {


    public static void main(String[] args) {
        LinkedList<String> list = new LinkedList<>();
        LinkedList<String> rList;

        System.out.println("Please enter 10 characters:");
        Scanner scanner = new Scanner(System.in);

        try {
            for (int counter = 0; counter < 10; counter++) {
                String input = scanner.next();
                list.add(input);
            }

            rList = revers(list);
            System.out.println("Reverse list is:");
            showList(rList);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static LinkedList<String> revers(LinkedList<String> charLlist) {
        LinkedList<String> reversList = new LinkedList<>();
        for (int i = 0; i < charLlist.size(); i++) {
            reversList.add(charLlist.get(charLlist.size() - i - 1));
        }
        return reversList;
    }


    private static void showList(LinkedList<String> charLlist) {
        for (String item : charLlist) {
            System.out.println(item);
        }
    }
}
