package exercise.chapter16;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;

public class Firstnames {


    public static void main(String[] args) {
        File nameList = new File("NamesLis.txt");
        HashSet<String> nameSet = new HashSet<>();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Reading file ...");
        try (BufferedReader reader = new BufferedReader(new FileReader(nameList))) {
            String line = reader.readLine();
            while (line != null) {
                nameSet.add(line);
                line = reader.readLine();
            }

            for (String s : nameSet) {
                System.out.println(s);
            }

            System.out.println("Enter your search key:");
            String searchKey = scanner.next();
            if (nameSet.contains(searchKey)) {
                System.out.println(searchKey + " is found in list of names");
            } else {
                System.out.println(searchKey + " does not found in list of names!");
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
