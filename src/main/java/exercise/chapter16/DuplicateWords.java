package exercise.chapter16;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class DuplicateWords {

    private static Map<String, Integer> myMap = new HashMap<>();
    private static boolean isRepeated;

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter string:");
        String input = scanner.nextLine();
        createMap(input);
        if (isRepeated) {
            for (String key : myMap.keySet()) {
                if (myMap.get(key) > 1) {
                    System.out.println(key + " repeated " + myMap.get(key) + " times.");
                }
            }
        } else {
            System.out.println("There is no duplicate word in the sentence!");
        }
    }


    private static void createMap(String input) {
        String[] tokens = input.replaceAll("[^a-zA-Z ]", "").toLowerCase().split(" ");
        int counter = 0;
        for (String word : tokens) {
            if (myMap.containsKey(word)) {
                int count = myMap.get(word);
                myMap.put(word, count + 1);
                counter++;
            } else {
                myMap.put(word, 1);
            }
        }
        if (counter == 0) {
            isRepeated = false;
        } else {
            isRepeated = true;
        }
    }

}
