package exercise.chapter17;

import java.util.ArrayList;
import java.util.List;

public class FindingPerson {

    public static void main(String[] args) {
        List<Employee> list = new ArrayList<>();
        EmployeeInfo.addEmployees(list);
        Employee person = list.stream()
                .filter(employee -> employee.getFirstName().equals("Jason"))
                .findFirst()
                .get();
        System.out.printf("The person found is %s %s.", person.getFirstName(), person.getLastName());
    }

}
