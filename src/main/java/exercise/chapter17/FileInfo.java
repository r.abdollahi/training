package exercise.chapter17;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class FileInfo {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please Enter directory name:");
        Path path = Paths.get(scanner.next());

        if (path.toFile().exists() && path.toFile().isDirectory()) {
            System.out.println("Directory contents:");
            try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(path)) {
                Stream<Path> stream = StreamSupport.stream(directoryStream.spliterator(), false);
                Map<String, Long> fileInfo = stream
                        .filter(fn -> !fn.toFile().isDirectory())
                        .map(fs -> fs.getFileName().toString())
                        .map(fe -> fe.substring(fe.lastIndexOf('.') + 1))
                        .collect(Collectors.groupingBy(String::toLowerCase
                                , TreeMap::new
                                , Collectors.counting()));
                fileInfo.forEach((key, value) -> System.out.printf("%n%s%s%s%s%n", ".", key, " count: ", value));
            }
        } else {
            System.out.printf("%n%s%n", "Directory is not valid!");
        }

    }
}
