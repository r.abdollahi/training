package exercise.chapter17;

import java.util.Scanner;
import java.util.regex.Pattern;


public class RemoveDuplicate {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please Enter sentence:");
        Pattern pattern = Pattern.compile(" ");

        System.out.printf("%n%s%n%n", "Removing duplicate words...");
        removingDuplicate(scanner.nextLine(), pattern);
    }


    public static void removingDuplicate(String input, Pattern pattern) {
        pattern.splitAsStream(input)
                .distinct()
                .forEach(System.out::println);
    }
}
