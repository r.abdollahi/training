package exercise.chapter17;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamOfLines {
    public static void main(String[] args) throws IOException {
        Pattern pattern = Pattern.compile("");
        Map<String, Long> wordCounts;
        try (Stream<String> lines = Files.lines(Paths.get("NamesList.txt"))) {
            wordCounts = lines
                    .flatMap(pattern::splitAsStream)
                    .collect(Collectors.groupingBy(
                            String::toLowerCase,
                            TreeMap::new, Collectors.counting()));
        }

        wordCounts.forEach((key, value) -> System.out.printf("%n%s:%s", key, value));


    }
}

