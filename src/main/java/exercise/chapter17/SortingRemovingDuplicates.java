package exercise.chapter17;

import java.security.SecureRandom;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.Character.*;

public class SortingRemovingDuplicates {
    private static final int lETTER_COUNT = 30;

    public static void main(String[] args) {
        SecureRandom rng = new SecureRandom();
        List<Character> charList = rng
                .ints(Character.MIN_CODE_POINT, MAX_CODE_POINT)
                .mapToObj(i -> (char) i)
                .filter(SortingRemovingDuplicates::filterChar)
                .limit(lETTER_COUNT)
                .collect(Collectors.toList());
        System.out.println("Random characters were generated...");

        System.out.println("Ascended sorted list is:");
        charList.stream().sorted().forEach(System.out::println);

        System.out.println("Descended sorted list is:");
        charList.stream().sorted(Comparator.reverseOrder()).forEach(System.out::println);

        System.out.println("Unique list is:");
        charList.stream().sorted().distinct().forEach(System.out::println);

    }


    private static boolean filterChar(char ch) {
        return ch >= '0' && ch <= 'z' && Character.isLetter(ch);
    }
}
