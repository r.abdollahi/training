package exercise.chapter17;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class DirectoryInfo {

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.printf("%n%s%n", "Please Enter directory name:");
        Path path = Paths.get(scanner.next());

        if (path.toFile().exists() && path.toFile().isDirectory()) {
            System.out.printf("%n%s%n", "Directory contents:");
            try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(path)) {
                directoryStream.forEach(file -> System.out.printf("%n%s%s%s%n"
                        , file.getFileName()
                        , " is a "
                        , file.toFile().isDirectory() ? "directory." : "file."));
            }
        } else {
            System.out.printf("%n%s%n", "Directory is not valid!");
        }

    }
}
