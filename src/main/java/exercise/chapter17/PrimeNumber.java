package exercise.chapter17;

import java.util.Scanner;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class PrimeNumber {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter count of prime number:");
        int limit = scanner.nextInt();

        Stream.iterate(0, n -> n + 1)
                .filter(PrimeNumber::isPrime)
                .limit(limit)
                .forEach(System.out::println);
    }


    private static boolean isPrime(int number) {
        if (number <= 1) return false;
        return IntStream
                .rangeClosed(2, (int) Math.sqrt(number)) // it's enough to check for numbers up to \/n
                .noneMatch(i -> number % i == 0);
    }
}
