package exercise.chapter17;

class Employee {

    private String firstName;
    private String lastName;
    private double salary;
    private String department;

    Employee(String firstName, String lastName, double salary, String department) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
        this.department = department;
    }


    double getSalary() {
        return salary;
    }


    String getFirstName() {
        return firstName;
    }


    String getLastName() {
        return lastName;
    }


    String getDepartment() {
        return department;
    }


}
