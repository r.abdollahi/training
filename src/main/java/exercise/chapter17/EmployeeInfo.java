package exercise.chapter17;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class EmployeeInfo {

    public static void main(String[] args) {
        List<Employee> list = new ArrayList<>();
        addEmployees(list);
        list.stream().collect(Collectors
                .groupingBy(Employee::getDepartment,
                        TreeMap::new,
                        Collectors.averagingDouble(Employee::getSalary)))
                .forEach((department, salary) ->
                        System.out.printf("%n Average salary of '%s' departmrnt is %s", department, salary));
    }


    static List<Employee> addEmployees(List<Employee> list) {
        list.add(new Employee("Jason", "Red", 5000, "IT"));
        list.add(new Employee("Ashley", "Green", 7600, "IT"));
        list.add(new Employee("Matthew", "Indigo", 3587.5, "Sales"));
        list.add(new Employee("James", "Indigo", 4700.77, "Marketing"));
        list.add(new Employee("Luke", "Indigo", 6200, "IT"));
        list.add(new Employee("Jason", "Blue", 3200, "Sales"));
        list.add(new Employee("Wendy", "Brown", 4236.4, "Marketing"));
        return list;
    }
}
