package exercise.chapter17;

import java.util.ArrayList;
import java.util.Scanner;

public class MapGrades {
    private static final int GRADE_COUNT = 100;

    public static void main(String[] args) {
        System.out.println("Please enter 10 grades:");
        Scanner scanner = new Scanner(System.in);
        int count = 0;
        ArrayList<Integer> gradeList = new ArrayList<>();

        try {
            while (count < GRADE_COUNT) {
                gradeList.add(scanner.nextInt());
                count++;
            }

            gradeList.forEach(g -> {
                if (g <= 20) System.out.printf("%n%s -> F", g);
                else if (g <= 40) System.out.printf("%n%s -> D", g);
                else if (g <= 60) System.out.printf("%n%s -> C", g);
                else if (g <= 80) System.out.printf("%n%s -> B", g);
                else if (g <= 100) System.out.printf("%n%s -> A", g);
                else System.out.printf("%n%s -> is not valid!", g);

            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}