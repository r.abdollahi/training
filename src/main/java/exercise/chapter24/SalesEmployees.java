package exercise.chapter24;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class SalesEmployees {

    private static String SELECT_QUERY = "SELECT * " +
            "FROM employees " +
            "WHERE departmentType LIKE 'sales'";

    public static void main(String[] arg) {
        try (Connection connection = new ConnectToEmployee().connect();
             Statement statment = connection.createStatement();
             ResultSet resultSet = statment.executeQuery(SELECT_QUERY)) {

            System.out.println("SALES Employees:");
            while (resultSet.next()) {
                for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                    System.out.printf(" %s", resultSet.getObject(i));
                }
                System.out.println();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
