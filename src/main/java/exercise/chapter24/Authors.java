package exercise.chapter24;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class Authors {

    private final static String SELECT_QUERY = "SELECT * FROM authors";

    public static void main(String[] args) {
        try (Connection connection = new ConnectToBooks().connect();
             Statement statment = connection.createStatement();
             ResultSet resultSet = statment.executeQuery(SELECT_QUERY)) {

            System.out.println("Authors list:");
            while (resultSet.next()) {
                for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                    System.out.printf(" %s", resultSet.getObject(i));
                }
                System.out.println();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}