package exercise.chapter24;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class EditAuthor {

    private static String UPDATE_QUERY = "UPDATE authors " +
            "SET firstname = ?" +
            "WHERE lastname = ?";


    public static void main(String[] args) {
        try (Connection connection = new ConnectToBooks().connect();
             PreparedStatement statement = connection.prepareStatement(UPDATE_QUERY)) {
            statement.setString(1, "David");
            statement.setString(2, "Red");
            statement.executeUpdate();

            System.out.println("One author was successfully Edited.");
        } catch (
                Exception e) {
            e.printStackTrace();
        }
    }
}
