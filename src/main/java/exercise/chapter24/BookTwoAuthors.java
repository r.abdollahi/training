package exercise.chapter24;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class BookTwoAuthors {
    private static String SELECT_QUERY = "SELECT * " +
            "FROM authors " +
            "INNER JOIN authorisbn " +
            "ON authors.id=authorisbn.authorid " +
            "WHERE authorisbn.isbnnum=0133807800 ";

    public static void main(String[] args) {
        try (Connection connection = new ConnectToBooks().connect();
             Statement statment = connection.createStatement();
             ResultSet resultSet = statment.executeQuery(SELECT_QUERY)) {

            System.out.println("0133807800 author:");
            while (resultSet.next()) {
                for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                    System.out.printf(" %s", resultSet.getObject(i));
                }
                System.out.println();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}