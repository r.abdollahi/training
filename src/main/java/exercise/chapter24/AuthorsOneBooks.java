package exercise.chapter24;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class AuthorsOneBooks {
    private static String SELECT_QUERY = "SELECT isbn,title,copyright " +
            "FROM titles " +
            "INNER JOIN authorisbn " +
            "ON titles.isbn=authorisbn.isbnNum " +
            "WHERE authorisbn.authorid=1";

    public static void main(String[] args) {
        try (Connection connection = new ConnectToBooks().connect();
             Statement statment = connection.createStatement();
             ResultSet resultSet = statment.executeQuery(SELECT_QUERY)) {

            System.out.println("Alis books list:");
            while (resultSet.next()) {
                for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                    System.out.printf(" %s", resultSet.getObject(i));
                }
                System.out.println();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
