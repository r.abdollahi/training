package exercise.chapter24;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class IncreaseBaseSalary {

    private static String UPDATE_QUERY = "UPDATE basepluscomissionemployees " +
            "SET basesalary = basesalary+(basesalary*0.1)";


    public static void main(String[] args) {
        try (Connection connection = new ConnectToEmployee().connect();
             PreparedStatement statement = connection.prepareStatement(UPDATE_QUERY)) {
            statement.executeUpdate();
            System.out.println("Base salary increased  by 10% for all base-plus-commission employees.");
        } catch (
                Exception e) {
            e.printStackTrace();
        }
    }
}
