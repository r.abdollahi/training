package exercise.chapter24;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class BusyEmployee {

    private static String SELECT_QUERY = "SELECT firstname,lastname, hours " +
            "FROM employees " +
            "INNER JOIN hourlyemployees " +
            "ON employees.id=hourlyemployees.id " +
            "WHERE hourlyemployees.hours >=30 ";

    public static void main(String[] args) {
        try (Connection connection = new ConnectToEmployee().connect();
             Statement statment = connection.createStatement();
             ResultSet resultSet = statment.executeQuery(SELECT_QUERY)) {

            System.out.println("Hourly employees working over 30 hours:");
            while (resultSet.next()) {
                for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                    System.out.printf(" %s", resultSet.getObject(i));
                }
                System.out.println();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
