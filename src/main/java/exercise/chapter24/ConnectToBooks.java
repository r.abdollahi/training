package exercise.chapter24;

import java.sql.Connection;
import java.sql.DriverManager;

class ConnectToBooks {

    private final String DATABASE_URL = "jdbc:mysql://localhost:3306/books?autoReconnect=true&useSSL=false";

    Connection connect() throws Exception {
        return DriverManager.getConnection(DATABASE_URL, "root", "");
    }
}