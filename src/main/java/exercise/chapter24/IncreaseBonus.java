package exercise.chapter24;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class IncreaseBonus {

    private static String UPDATE_HOUR_EMPLOYEES = "UPDATE hourlyemployees he " +
            "INNER JOIN employees e " +
            "ON he.id=e.id  " +
            "SET he.bonus= he.bonus+100000 " +
            "WHERE MONTH(e.birthday)=MONTH(NOW())";

    private static String UPDATE_BASE_PLUSECOMMISSION_EMPLOYEE = "UPDATE basepluscomissionemployees be " +
            "INNER JOIN employees e " +
            "ON be.id=e.id  " +
            "SET be.bonus=be.bonus+100000  " +
            "WHERE MONTH(e.birthday)=MONTH(NOW()) ";

    private static String UPDATE_COMMISSION_EMPLOYEES = "UPDATE commissionemployees ce " +
            "INNER JOIN employees e  " +
            "ON ce.id=e.id  " +
            "SET ce.bonus=ce.bonus+100000 " +
            "WHERE MONTH(e.birthday)=MONTH(NOW())";

    private static String UPDATE_SALARIED_EMPLOYEES = "UPDATE salariedemployees se " +
            "INNER JOIN employees e " +
            "ON se.id=e.id " +
            "SET se.bonus=se.bonus+100000 " +
            "WHERE MONTH(e.birthday)=MONTH(NOW())";

    public static void main(String[] arhs) {
        try (Connection connection = new ConnectToEmployee().connect();
             PreparedStatement updateHourEmployees = connection.prepareStatement(UPDATE_HOUR_EMPLOYEES);
             PreparedStatement updateCommissionEmployees = connection.prepareStatement(UPDATE_COMMISSION_EMPLOYEES);
             PreparedStatement updateBasePluscommissionEmployees = connection.prepareStatement(UPDATE_BASE_PLUSECOMMISSION_EMPLOYEE);
             PreparedStatement updateSalariedEmployees = connection.prepareStatement(UPDATE_SALARIED_EMPLOYEES)) {

            updateHourEmployees.executeUpdate();
            updateCommissionEmployees.executeUpdate();
            updateBasePluscommissionEmployees.execute();
            updateSalariedEmployees.executeUpdate();

            System.out.println("Bonus increased for Employees that born this month.");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
