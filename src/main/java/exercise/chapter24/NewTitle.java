package exercise.chapter24;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class NewTitle {

    private static String INSERT_QUERY_TO_TITLE = "INSERT INTO titles (isbn,title,editionnumber,copyright) " +
            "VALUES (?,?,?,?)";
    private static String INSERT_QUERY_TO_AUTHORISBN = "INSERT INTO authorisbn (authorid,isbnnum) " +
            "VALUES (?,?)";


    public static void main(String[] args) {
        try (Connection connection = new ConnectToBooks().connect();
             PreparedStatement statement1 = connection.prepareStatement(INSERT_QUERY_TO_TITLE)) {

            statement1.setInt(1, 1296009208);
            statement1.setString(2, "Head First Java");
            statement1.setInt(3, 2);
            statement1.setString(4, "2018");
            statement1.execute();

            System.out.println("The new Book was successfully inserted.");

            try (PreparedStatement statement2 = connection.prepareStatement(INSERT_QUERY_TO_AUTHORISBN)) {

                statement2.setInt(1, 1);
                statement2.setInt(2, 1296009208);
                statement2.execute();

                System.out.println("The new Book was successfully related to its author.");

            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
