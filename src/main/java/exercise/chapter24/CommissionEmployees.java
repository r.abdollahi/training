package exercise.chapter24;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class CommissionEmployees {
    private static String SELECT_QUERY = "SELECT firstname,lastname, commissionrate " +
            "FROM employees " +
            "INNER JOIN commissionemployees " +
            "ON employees.id=commissionemployees.id " +
            "ORDER BY commissionrate DESC";

    public static void main(String[] args) {
        try (Connection connection = new ConnectToEmployee().connect();
             Statement statment = connection.createStatement();
             ResultSet resultSet = statment.executeQuery(SELECT_QUERY)) {

            System.out.println("Commission employees list:");
            while (resultSet.next()) {
                for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                    System.out.printf(" %s", resultSet.getObject(i));
                }
                System.out.println();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}