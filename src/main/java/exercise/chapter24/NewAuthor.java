package exercise.chapter24;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class NewAuthor {

    private static String INSERT_QUERY = "INSERT INTO authors (firstname,lastname) " +
            "VALUES (?, ?)";


    public static void main(String[] args) {
        try (Connection connection = new ConnectToBooks().connect();
             PreparedStatement statement = connection.prepareStatement(INSERT_QUERY)) {
            statement.setString(1, "Jack");
            statement.setString(2, "Petter");
            statement.execute();
            System.out.println("The new author was successfully inserted.");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
