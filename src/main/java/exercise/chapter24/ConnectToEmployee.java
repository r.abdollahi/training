package exercise.chapter24;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

class ConnectToEmployee {
    private final String DATABASE_URL = "jdbc:mysql://localhost:3306/employee?autoReconnect=true&useSSL=false";


    Connection connect() throws SQLException {
        return DriverManager.getConnection(DATABASE_URL, "root", "");
    }
}
