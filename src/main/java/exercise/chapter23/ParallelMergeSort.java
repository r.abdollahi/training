package exercise.chapter23;

import java.security.SecureRandom;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

public class ParallelMergeSort extends RecursiveAction {

    private int[] data;
    private int low;
    private int high;


    private ParallelMergeSort(int[] data, int low, int high) {
        this.data = data;
        this.low = low;
        this.high = high;
    }


    public static void main(String[] args) {
        SecureRandom generator = new SecureRandom();
        int[] list = generator.ints(10, 10, 91).toArray();
        ForkJoinPool pool = new ForkJoinPool();
        ParallelMergeSort mergeSort = new ParallelMergeSort(list, 0, list.length - 1);

        System.out.printf("Unsorted array: %s%n%n", Arrays.toString(list));

        Instant startTime = Instant.now();
        pool.submit(mergeSort).join();
        Instant endTime = Instant.now();

        long timeDuration = Duration.between(startTime, endTime).toMillis();
        System.out.printf("Sorted array: %s%n", Arrays.toString(list));
        System.out.println("\nParallel calculation time duration: " + timeDuration + " milliseconds");
    }


    @Override
    protected void compute() {
        if ((high - low) >= 1) {
            int middle1 = (low + high) / 2;
            int middle2 = middle1 + 1;

            invokeAll(new ParallelMergeSort(data, low, middle1), new ParallelMergeSort(data, middle2, high));
            merge(data, low, middle1, middle2, high);
        }
    }


    private static void merge(int[] data, int left, int middle1, int middle2, int right) {
        int leftIndex = left;
        int rightIndex = middle2;
        int combinedIndex = left;
        int[] combined = new int[data.length];

        while (leftIndex <= middle1 && rightIndex <= right) {

            if (data[leftIndex] <= data[rightIndex]) {
                combined[combinedIndex++] = data[leftIndex++];
            } else {
                combined[combinedIndex++] = data[rightIndex++];
            }
        }

        if (leftIndex == middle2) {

            while (rightIndex <= right) {
                combined[combinedIndex++] = data[rightIndex++];
            }
        } else {

            while (leftIndex <= middle1) {
                combined[combinedIndex++] = data[leftIndex++];
            }
        }

        if (right + 1 - left >= 0) {
            System.arraycopy(combined, left, data, left, right + 1 - left);
        }

    }

}
