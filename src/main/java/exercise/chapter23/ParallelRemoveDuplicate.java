package exercise.chapter23;

import exercise.chapter17.RemoveDuplicate;

import java.time.Duration;
import java.time.Instant;
import java.util.Scanner;
import java.util.regex.Pattern;

public class ParallelRemoveDuplicate {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please Enter sentence:");
        Pattern pattern = Pattern.compile(" ");
        String input = scanner.nextLine();

        Instant startTime = Instant.now();
        RemoveDuplicate.removingDuplicate(input, pattern);
        Instant endTime = Instant.now();
        long timeDuration = Duration.between(startTime, endTime).toMillis();
        System.out.println("\nSerial calculation time duration: " + timeDuration + " milliseconds");

        System.out.printf("%n%s%n%n", "Removing duplicate words...");
        Instant parallelStartTime = Instant.now();
        pattern.splitAsStream(input)
                .parallel()
                .distinct()
                .forEach(System.out::println);
        Instant parallelEndTime = Instant.now();
        long parallelTimeDuration = Duration.between(parallelStartTime, parallelEndTime).toMillis();
        System.out.println("\nParallel calculation time duration: " + parallelTimeDuration + " milliseconds");
    }
}
