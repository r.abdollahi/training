package exercise.chapter23;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class CircularBuffer implements Buffer {
    private final int[] buffer = {-1, -1, -1};
    private int occupiedCells = 0;
    private int writeIndex = 0;
    private int readIndex = 0;

    private final Lock ACCESS_LOCK = new ReentrantLock();
    private final Condition CAN_WRITE = ACCESS_LOCK.newCondition();
    private final Condition CAN_READ = ACCESS_LOCK.newCondition();


    // place value into buffer
    @Override
    public void blockingPut(int value) throws InterruptedException {
        try {
            ACCESS_LOCK.lock();
            while (occupiedCells == buffer.length) {
                System.out.printf("Buffer is full. Producer waits.%n");
                CAN_WRITE.await();
            }

            buffer[writeIndex] = value;
            writeIndex = (writeIndex + 1) % buffer.length;
            ++occupiedCells;
            displayState("Producer writes " + value);

            CAN_WRITE.notifyAll();
        } finally {
            ACCESS_LOCK.unlock();
        }
    }


    // return value from buffer
    @Override
    public int blockingGet() throws InterruptedException {
        int readValue;
        try {
            ACCESS_LOCK.lock();
            while (occupiedCells == 0) {
                System.out.printf("Buffer is empty. Consumer waits.%n");
                CAN_READ.await();
            }

            readValue = buffer[readIndex];
            readIndex = (readIndex + 1) % buffer.length;
            --occupiedCells;
            displayState("Consumer reads " + readValue);

            CAN_READ.notifyAll();
        } finally {
            ACCESS_LOCK.unlock();
        }
        return readValue;
    }


    // display current operation and buffer state
    public void displayState(String operation) {
        try {
            ACCESS_LOCK.lock();
            System.out.printf("%s%s%d)%n%s", operation, " (buffer cells occupied: ", occupiedCells, "buffer cells:  ");

            for (int value : buffer) {
                System.out.printf(" %2d  ", value);
            }

            System.out.printf("%n               ");

            for (int i = 0; i < buffer.length; i++) {
                System.out.print("---- ");
            }

            System.out.printf("%n               ");

            for (int i = 0; i < buffer.length; i++) {
                if (i == writeIndex && i == readIndex) {
                    System.out.print(" WR");
                } else if (i == writeIndex) {
                    System.out.print(" W  ");
                } else if (i == readIndex) {
                    System.out.print("  R  ");
                } else {
                    System.out.print("  ");
                }
            }

            System.out.printf("%n%n");
        } finally {
            ACCESS_LOCK.unlock();
        }
    }
}
