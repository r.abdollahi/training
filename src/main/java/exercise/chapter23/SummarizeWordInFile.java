package exercise.chapter23;

import exercise.chapter17.StreamOfLines;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SummarizeWordInFile {
    public static void main(String[] args) throws IOException {
        Instant startTime = Instant.now();
        StreamOfLines.main(null);
        Instant endTime = Instant.now();

        long timeDuration = Duration.between(startTime, endTime).toMillis();
        System.out.println("\n\n\n Serial calculation time duration: " + timeDuration + " milliseconds");

        Pattern pattern = Pattern.compile("\\s");
        Map<String, Long> wordCounts;
        Instant parallelStartTime = Instant.now();
        try (Stream<String> lines = Files.lines(Paths.get("NamesList.txt"))) {
            wordCounts = lines
                    .parallel()
                    .flatMap(pattern::splitAsStream)
                    .collect(Collectors.groupingBy(
                            String::toLowerCase,
                            TreeMap::new, Collectors.counting()));
        }
        Instant parallelEndTime = Instant.now();

        long parallelTimeDuration = Duration.between(parallelStartTime, parallelEndTime).toMillis();
        wordCounts.forEach((key, value) -> System.out.printf("%n%s:%s%n", key, value));
        System.out.println("\n\n\n Parallel calculation time duration: " + parallelTimeDuration + " milliseconds");
    }
}

