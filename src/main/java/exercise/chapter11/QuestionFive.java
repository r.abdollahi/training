package exercise.chapter11;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.TreeMap;

public class QuestionFive {

    public static void main(String[] args) {
        File dataFile = new File("phoneBookData.xml");
        Map<String, String> phonebookTree;

        if (!dataFile.exists()) {
            System.out.println("No phone book data file found!");
            System.out.println("File name: " + dataFile.getAbsolutePath());
            System.out.println("");
        } else {
            try {
                phonebookTree = readPhonbook(dataFile);
                changePhonebook(phonebookTree);
                writePhonbook(dataFile, phonebookTree);
            } catch (Exception e) {
                System.out.println(e);
            }
        }

    }


    private static Map<String, String> readPhonbook(File file) throws Exception {
        Map<String, String> phoneBook = new TreeMap<>();
        Document xmlDoc;
        Element rootElement;
        DocumentBuilder docReader;

        System.out.println("Reading file " + file.getAbsolutePath() + " ...");
        System.out.println("");

        docReader = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        xmlDoc = docReader.parse(file);
        rootElement = xmlDoc.getDocumentElement();
        if (!rootElement.getNodeName().equals("phone_directory")) {
            throw new Exception("File is not a phonebook data file.");
        } else {
            NodeList nodes = rootElement.getChildNodes();
            for (int i = 0; i < nodes.getLength(); i++) {
                if (nodes.item(i) instanceof Element) {
                    Element subElement = (Element) nodes.item(i);
                    phoneBook.put(subElement.getAttribute("name"), subElement.getAttribute("number"));
                }
            }
        }
        return phoneBook;
    }


    private static Map<String, String> changePhonebook(Map<String, String> phoneBook) {
        //You can change phonebook content
        for (Map.Entry<String, String> entry : phoneBook.entrySet()) {
            if (entry.getKey().equals("barney")) {
                entry.setValue("100-520");
                System.out.println("Changing phonebook content... ");
                System.out.println("");
                break;
            }
        }
        return phoneBook;
    }

    private static void writePhonbook(File file, Map<String, String> phonbook) {
        System.out.println("Saving phone directory changes to file " + file.getAbsolutePath() + " ...");
        try (PrintWriter printer = new PrintWriter(file)) {
            printer.println("<?xml version=\"1.0\"?>");
            printer.println("<phone_directory>");
            for (Map.Entry<String, String> entry : phonbook.entrySet()) {
                printer.println("<entry name='" + entry.getKey() + "' number='" + entry.getValue() + "'/>");
            }
            printer.println("</phone_directory>");
            printer.flush();
            System.out.println("Done!");
        } catch (IOException e) {
            System.out.println("ERROR: Some error occurred while writing data file.");
        }
    }


}
