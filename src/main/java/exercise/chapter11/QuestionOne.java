package exercise.chapter11;

import java.io.File;
import java.util.Scanner;

public class QuestionOne {
    public static void main(String[] args) {
        String directoryName;
        File directory;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter a directory name to show directory list:");
        directoryName = scanner.nextLine().trim();
        directory = new File(directoryName);

        if (!directory.isDirectory()) {
            if (!directory.exists()) {
                System.out.print("There is no such directory!");
            } else {
                System.out.println("There is not a directory!");
            }
        } else
            printDirContent(directory);
    }


    public static void printDirContent(File directory) {
        String[] files = directory.list();
        for (int i = 0; i < files.length; i++) {
            File filename = new File(directory, files[i]);
            if (!filename.isDirectory())
                System.out.println(filename.getPath());
            else {
                System.out.println(filename.getPath());
                printDirContent(filename);
            }
        }
    }
}
