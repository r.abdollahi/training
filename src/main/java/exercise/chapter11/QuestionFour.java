package exercise.chapter11;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class QuestionFour {
    public static final int DEFAULT_PORT = 1732;
    public static final String IP_ADDRESS = "127.0.0.1";

    public static void main(String[] aregs) {
        String outgoing;
        PrintWriter sendMassage;
        BufferedReader recieveMassage;


        try (Socket connection = new Socket(IP_ADDRESS, DEFAULT_PORT)) {
            System.out.println("Connect to " + IP_ADDRESS + " on port " + DEFAULT_PORT + " :");
            sendMassage = new PrintWriter(connection.getOutputStream());

            recieveMassage = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String command = getCommand();

            boolean index;
            if (InterpretCommand.validate(command))
                index = false;
            else if (command.equals("INDEX"))
                index = true;
            else {
                System.out.println("Command is not valid!");
                return;
            }

            outgoing = command;
            sendMassage.println(outgoing);
            sendMassage.flush();

            if (index) {
                System.out.println("List of files on the server:");
                while (true) {
                    String data = recieveMassage.readLine();
                    if (data.equals(""))
                        break;
                    System.out.println(data);
                }
            } else {
                String fileName;
                System.out.println("File received...");
                fileName = InterpretCommand.extractAddress(command);
                copyFile(connection.getInputStream(), fileName);
            }
        } catch (Exception e) {
            System.out.println("Error occurred server connection.");
            System.out.println(e);

        }

    }


    public static String getCommand() {
        System.out.println("Please enter \"INDEX\" command to get files list or enter GET<filename> command format to copy specific file:");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine().trim();
    }


    public static void copyFile(InputStream source, String fileName) throws IOException {
        File file = new File(fileName);
        if (file.exists()) {
            System.out.println("The file could not be copied because this is exist in current directory!");
            return;
        }
        OutputStream outputStream = new FileOutputStream(file);

        while (true) {
            int data = source.read();
            if (data < 0)
                break;
            outputStream.write(data);
        }
        System.out.println("The file was copied in current directory successfully");
        outputStream.close();
    }
}
