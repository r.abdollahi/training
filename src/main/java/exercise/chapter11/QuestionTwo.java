package exercise.chapter11;

import java.io.*;

public class QuestionTwo {
    public static void main(String[] args) {
        int lineCount;
        BufferedReader in;
        String line = "";

        System.out.println("Please enter file path (with extension) and press CTRL+D to end-of-stream:");
        in = new BufferedReader(new InputStreamReader(System.in));
        try {
            line = in.readLine();
            while (line != null) {
                lineCount = processOneLineOfInput(line);
                System.out.println("number of lines in file: " + lineCount);
                line = in.readLine();
            }
        } catch (IOException e) {
            System.out.println("An error occurred in file processing: " + line);
        }
    }


    private static int processOneLineOfInput(String file) throws IOException {
        int lineCount = 0;
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            while (true) {
                String line = reader.readLine();
                if (line == null) {
                    break;
                }
                lineCount++;
            }
        }
        return lineCount;

    }
}

