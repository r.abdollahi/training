package exercise.chapter11;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class QuestionThree {
    public static final int DEFAULT_PORT = 1732;

    public static void main(String[] args) {
        Socket connection;
        BufferedReader receiveMassage;
        PrintWriter sendMassage;
        String incoming;
        String outgoing;
        File filesDirectory;

        try {
            filesDirectory = getDirectory();
            if (filesDirectory == null) {
                return;
            }
            try (ServerSocket listener = new ServerSocket(DEFAULT_PORT)) {
                System.out.println("Listen on port: " + listener.getLocalPort());
                connection = listener.accept();
            }

            receiveMassage = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            sendMassage = new PrintWriter(connection.getOutputStream());

        } catch (Exception e) {
            System.out.println(e.toString());
            return;
        }

        try {
            incoming = receiveMassage.readLine().trim();
            System.out.println("RECEIVED: " + incoming);
            if (incoming.equals("INDEX")) {
                outgoing = getFilesName(filesDirectory);
                sendMassage.println(outgoing);
                sendMassage.flush();
                connection.close();
            } else if (InterpretCommand.validate(incoming)) {
                String Address;
                String content;
                Address = InterpretCommand.extractAddress(incoming);
                content = readFile(filesDirectory + "\\" + Address);
                sendMassage.println(content);
                sendMassage.flush();
                if (sendMassage.checkError()) {
                    throw new IOException("Error occurred while transmitting message.");
                }
                connection.close();

            } else {
                sendMassage.println("Command is not valid!");
                sendMassage.flush();
                if (sendMassage.checkError()) {
                    throw new IOException("Error occurred while transmitting message.");
                }
                connection.close();
            }

        } catch (Exception e) {
            sendMassage.println(e);
            sendMassage.flush();
        }

    }


    public static File getDirectory() {
        String directory;
        File file;
        System.out.println("Please Enter your files directory:");
        Scanner scanner = new Scanner(System.in);
        directory = scanner.nextLine().trim();
        file = new File(directory);
        if (!file.isDirectory()) {
            System.out.print("There is no such directory!");
            return null;
        } else
            return file;
    }


    public static String readFile(String fileName) throws IOException {
        String fileContent = "";
        File file = new File(fileName);
        try (FileReader fr = new FileReader(file)) {
            char[] chars = new char[(int) file.length()];
            fr.read(chars);
            fileContent = new String(chars);
        }
        return fileContent;
    }


    public static String getFilesName(File directory) {
        StringBuilder fileList = new StringBuilder();
        for (int i = 0; i < directory.list().length; i++) {
            fileList.append(directory.list()[i]);
            fileList.append("\n");
        }
        return fileList.toString();
    }
}
