package exercise.chapter11;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InterpretCommand {
    public static boolean validate(String command) {
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile("GET<.*>");
        matcher = pattern.matcher(command);
        return matcher.matches();
    }


    public static String extractAddress(String command) {
        return command.substring(4, command.length() - 1);
    }
}
