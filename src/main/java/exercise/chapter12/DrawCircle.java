package exercise.chapter12;

import java.util.Random;

public class DrawCircle {

    private static final int THREAD_COUNT = 7;
    private static final int BATCH_SIZE = 1000000;
    private static long inCircleCount;
    private static long trialCount;


    public static void main(String[] args) {
        System.out.println("Circle drawing program started with " + THREAD_COUNT + " thread count...");
        generatePoint[] thread = new generatePoint[THREAD_COUNT];

        try {
            for (int i = 0; i <= thread.length; i++) {
                thread[i] = new generatePoint();
                thread[i].start();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private static class generatePoint extends Thread {

        @Override
        public void run() {
            Random rnd = new Random();
            while (true) {
                int inside = 0;
                for (int i = 1; i <= BATCH_SIZE; i++) {
                    double x = rnd.nextDouble();
                    double y = rnd.nextDouble();
                    if (x * x + y * y < 1) {
                        inside++;
                    }
                }
                synchronized (Thread.currentThread()) {
                    trialCount += BATCH_SIZE;
                    inCircleCount += inside;

                    double estimateForPi = 4 * ((double) inCircleCount / trialCount);
                    System.out.println("Number of Trials: " + trialCount + " Current Estimate: " + estimateForPi + "\n");
                }
            }
        }
    }

}
