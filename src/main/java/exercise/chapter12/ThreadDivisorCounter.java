package exercise.chapter12;

import java.util.Scanner;

public class ThreadDivisorCounter extends Thread {
    private int NStart;
    private int NEnd;
    private int[] divisorList;


    private ThreadDivisorCounter(int start, int end, int[] list) {
        NStart = start;
        NEnd = end;
        divisorList = list;
    }

    @Override
    public void run() {
        for (int i = NStart; i <= NEnd; i++) {
            counter(i, divisorList);
        }
    }


    public static void main(String[] args) {
        int N = 0;
        int threadCount = 0;
        Scanner scanner;

        try {
            scanner = new Scanner(System.in);
            while (N <= 0) {
                System.out.println("Please enter positive value as N: ");
                N = Integer.parseInt(scanner.nextLine());
            }

            while (threadCount <= 0) {
                System.out.println("Please enter thread count: ");
                threadCount = Integer.parseInt(scanner.nextLine());
            }

            System.out.println("Please wait... ");
            int[] list = new int[N];
            long startTime = System.currentTimeMillis();
            ThreadDivisorCounter[] thread = new ThreadDivisorCounter[threadCount];
            int start;
            int end = 0;
            int section = 0;

            for (int i = 0; i < thread.length; i++) {
                section++;
                start = end + 1;
                if (section == threadCount) {
                    end = N;
                } else {
                    end = N / threadCount * section;
                }
                thread[i] = new ThreadDivisorCounter(start, end, list);
                thread[i].start();
            }
            for (ThreadDivisorCounter threadDivisorCounter : thread) {
                threadDivisorCounter.join();
            }

            int[] maxValue = findMax(list);
            long elapsedTime = System.currentTimeMillis() - startTime;

            System.out.println("The time elapsed is " + (elapsedTime / 1000.0) + " seconds.");
            System.out.println(maxValue[0] + " has " + maxValue[1] + " divisor(s)");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static void counter(int N, int[] list) {
        int counter = 0;
        for (int i = 1; i < N; i++) {
            if (N % i == 0) {
                counter++;
            }
        }
        list[N - 1] = counter;
    }


    private static int[] findMax(int[] list) {
        int max = list[0];
        int index = 1;
        for (int i = 1; i < list.length; i++) {
            if (list[i] > max) {
                max = list[i];
                index = i + 1;
            }
        }
        return new int[]{index, max};
    }

}
