package exercise.chapter12;


import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedQueue;

public class QueueThreadDivisorCounter {
    private static ConcurrentLinkedQueue<CalculateDivisor> taskQueue;
    private static int[] divisorList;
    private static final int INTERVAL = 2000;
    private static final int N = 100000;


    public static void main(String[] args) {
        int threadCount = 0;
        Scanner scanner;
        divisorList = new int[N];
        taskQueue = new ConcurrentLinkedQueue<>();

        try {
            scanner = new Scanner(System.in);
            while (threadCount <= 0) {
                System.out.println("Please enter thread count: ");
                threadCount = Integer.parseInt(scanner.nextLine());
            }

            System.out.println("Please wait... ");
            long startTime = System.currentTimeMillis();
            fillQueue();

            WorkerThread[] thread = new WorkerThread[threadCount];
            for (int i = 0; i < thread.length; i++) {
                thread[i] = new WorkerThread();
                thread[i].start();
            }

            for (WorkerThread t : thread) {
                t.join();
            }

            int[] max = findMax(divisorList);
            long elapsedTime = System.currentTimeMillis() - startTime;
            System.out.println("The time elapsed is " + (elapsedTime / 1000.0) + " seconds.");
            System.out.println(max[0] + " has " + max[1] + " divisor(s)");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static void counter(int start, int end, int number) {
        int counter = 0;
        for (int i = start; i <= end; i++) {
            if (number % i == 0 && number != i) {
                counter++;
            }
        }
        synchronized (Thread.currentThread()) {
            divisorList[number - 1] += counter;
        }
    }


    private static void fillQueue() {
        for (int j = 1; j <= N; j++) {
            if (j <= INTERVAL) {
                taskQueue.add(new CalculateDivisor(1, j, j));
            } else {
                int part = j / INTERVAL;
                if (j % INTERVAL != 0) {
                    part++;
                }
                for (int i = 1; i <= part; i++) {
                    taskQueue.add(new CalculateDivisor((INTERVAL * i) - INTERVAL + 1, min(INTERVAL * i, j), j));
                }
            }
        }
    }


    private static int min(int num1, int num2) {
        if (num1 < num2) {
            return num1;
        } else {
            return num2;
        }
    }


    private static int[] findMax(int[] list) {
        int max = list[0];
        int index = 1;

        for (int i = 1; i < list.length; i++) {
            if (list[i] > max) {
                max = list[i];
                index = i + 1;
            }
        }
        return new int[]{index, max};
    }


    private static class CalculateDivisor implements Runnable {
        private int startPoint;
        private int endPoint;
        private int number;

        private CalculateDivisor(int start, int end, int num) {
            startPoint = start;
            endPoint = end;
            number = num;
        }

        @Override
        public void run() {
            counter(startPoint, endPoint, number);
        }

    }


    private static class WorkerThread extends Thread {

        @Override
        public void run() {
            CalculateDivisor task = taskQueue.poll();
            while (task != null) {
                task.run();
                task = taskQueue.poll();
            }
        }

    }

}
