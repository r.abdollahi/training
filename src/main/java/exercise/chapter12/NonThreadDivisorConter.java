package exercise.chapter12;

import java.util.Scanner;


public class NonThreadDivisorConter {
    static int divisorCount;
    static int maxValue;


    private static void setDivisorCount(int count) {
        divisorCount = count;
    }


    private static int getDivisorCount() {
        return divisorCount;
    }


    private static void setMaxValue(int value) {
        maxValue = value;
    }


    private static int getMaxValue() {
        return maxValue;
    }


    public static void main(String[] args) {
        int N = 0;
        Scanner scanner;

        try {
            scanner = new Scanner(System.in);
            while (N <= 0) {
                System.out.println("Please enter positive value as N: ");
                N = Integer.parseInt(scanner.nextLine());
            }

            System.out.println("Please wait... ");
            long startTime = System.currentTimeMillis();
            for (int i = 0; i < N; i++) {
                counter(i);
            }

            System.out.println("Please wait... ");
            long elapsedTime = System.currentTimeMillis() - startTime;
            System.out.println("The time elapsed is " + (elapsedTime / 1000.0) + " seconds.");
            System.out.println(getMaxValue() + " has " + getDivisorCount() + " divisor(s)");

        } catch (Exception e) {
            System.out.println(e);
        }
    }


    private static void counter(int N) {
        int counter = 0;
        for (int i = 1; i < N; i++) {
            if (N % i == 0) {
                counter++;
            }
        }
        if (counter > getDivisorCount()) {
            setDivisorCount(counter);
            setMaxValue(N);
        }
    }

}

