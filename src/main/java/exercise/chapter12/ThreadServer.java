package exercise.chapter12;

import exercise.chapter11.InterpretCommand;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class ThreadServer {

    private static final int DEFAULT_PORT = 1732;
    private static File filesDirectory;

    public static void main(String[] args) {
        Socket connection;

        try {
            filesDirectory = getDirectory();
            if (filesDirectory == null) {
                return;
            }
            try (ServerSocket listener = new ServerSocket(DEFAULT_PORT)) {
                System.out.println("Listen on port: " + listener.getLocalPort());
                while (true) {
                    connection = listener.accept();
                    ConnectionHandler handler = new ConnectionHandler(connection);
                    handler.start();
                }
            }

        } catch (Exception e) {
            System.out.println(e.toString());
            return;
        }

    }


    private static class ConnectionHandler extends Thread {
        Socket connection;

        ConnectionHandler(Socket socket) {
            connection = socket;
        }

        @Override
        public void run() {
            BufferedReader receiveMassage;
            PrintWriter sendMassage = null;
            String incoming;
            String outgoing;

            try {
                receiveMassage = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                sendMassage = new PrintWriter(connection.getOutputStream());

                incoming = receiveMassage.readLine().trim();
                System.out.println("RECEIVED: " + incoming);

                if (incoming.equals("INDEX")) {
                    outgoing = getFilesName(filesDirectory);
                    sendMassage.println(outgoing);
                    sendMassage.flush();
                    connection.close();
                } else if (InterpretCommand.validate(incoming)) {
                    String Address;
                    String content;
                    Address = InterpretCommand.extractAddress(incoming);
                    content = readFile(filesDirectory + "\\" + Address);
                    sendMassage.println(content);
                    sendMassage.flush();
                    if (sendMassage.checkError()) {
                        throw new IOException("Error occurred while transmitting message.");
                    }
                    connection.close();

                } else {
                    sendMassage.println("Command is not valid!");
                    sendMassage.flush();
                    if (sendMassage.checkError()) {
                        throw new IOException("Error occurred while transmitting message.");
                    }
                    connection.close();
                }

            } catch (Exception e) {
                sendMassage.println(e);
                sendMassage.flush();
            }
        }
    }


    private static File getDirectory() {
        String directory;
        File file;
        System.out.println("Please Enter your files directory:");
        Scanner scanner = new Scanner(System.in);
        directory = scanner.nextLine().trim();
        file = new File(directory);
        if (!file.isDirectory()) {
            System.out.print("There is no such directory!");
            return null;
        } else
            return file;
    }


    private static String readFile(String fileName) throws IOException {
        String fileContent = "";
        File file = new File(fileName);
        try (FileReader fr = new FileReader(file)) {
            char[] chars = new char[(int) file.length()];
            fr.read(chars);
            fileContent = new String(chars);
        }
        return fileContent;
    }


    private static String getFilesName(File directory) {
        StringBuilder fileList = new StringBuilder();
        for (int i = 0; i < directory.list().length; i++) {
            fileList.append(directory.list()[i]);
            fileList.append("\n");
        }
        return fileList.toString();
    }

}
