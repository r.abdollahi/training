package exercise.chapter12;


import java.util.Scanner;

public class CallCounter extends Thread {
    private int time;

    private CallCounter(int timeCount) {
        time = timeCount;
    }

    @Override
    public void run() {
        for (int i = 0; i < time; i++) {
            Counter.inc();
        }
    }

    public static class Counter {
        private static int count;

        static void inc() {
            count = count + 1;
        }

        static int getCounter() {
            return count;
        }
    }

    public static void main(String[] args) {
        int threadCount = 0;
        int countTime = 0;
        try {
            System.out.println("How many threads do you want to use? ");
            Scanner scanner = new Scanner(System.in);
            threadCount = Integer.parseInt(scanner.nextLine());

            System.out.println("How many times should the counter be counted? ");
            countTime = Integer.parseInt(scanner.nextLine());

            if (threadCount == 0 || countTime == 0) {
                throw new Exception();
            }

            CallCounter[] worker = new CallCounter[threadCount];
            for (int i = 0; i < threadCount; i++) {
                worker[i] = new CallCounter(countTime);
                worker[i].start();
            }


            for (int i = 0; i < threadCount; i++) {
                try {
                    worker[i].join();
                } catch (InterruptedException e) {
                    System.out.println("Thread interruption error!");
                }
            }

            System.out.println("Counter is: " + CallCounter.Counter.getCounter());
        } catch (Exception e) {
            System.out.println("Input is not correct!");
        }

    }

}
