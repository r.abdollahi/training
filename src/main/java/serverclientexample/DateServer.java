package serverclientexample;

import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

public class DateServer {
    private static final int LISTENING_PORT = 32007;

    public static void main(String[] args) {
        Socket connection;

        try (ServerSocket listener=new ServerSocket(LISTENING_PORT)){
            System.out.println("Listening on port... " + LISTENING_PORT);
            while (true) {
                connection = listener.accept();
                sendDate(connection);
            }
        }
        catch(Exception e){
            System.out.println("\"Sorry, the server has shut down.\"");
            System.out.println("Error: " + e);
            return;
        }
    }

    private static void sendDate(Socket client) {
        try {
            System.out.println("Connection from " + client.getInetAddress().toString());
            Date now = new Date();
            PrintWriter outgoing;
            outgoing = new PrintWriter(client.getOutputStream());
            outgoing.println(now.toString());
            outgoing.flush();
            client.close();
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }

    }
}
