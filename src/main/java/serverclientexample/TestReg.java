package serverclientexample;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestReg {

    public static void main(String[] args) {
        String c = "GET<I:\\test\\A>";
        boolean ismatch = validateCommand(c);
        String address=regSubstring(c);
        System.out.println(ismatch);
        System.out.println(address);
    }

    private static boolean validateCommand(String command) {
        Pattern pattern;
        Matcher matcher;

        pattern = Pattern.compile("GET<.*>");
        matcher = pattern.matcher(command);
        return matcher.matches();
    }


    private static String regSubstring(String command) {
//        String pattern;
        String substr;
//        pattern = "GET<.*>";
        substr=command.substring(4,command.length()-2);
        return substr ;
    }
}
