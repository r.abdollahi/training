package serverclientexample;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;

public class QuestionThreeClient {
    public static final int LISTENING_PORT = 1732;

    public static void main(String[] args) {
        String hostName = "127.0.0.1";
        Socket connection;
        BufferedReader incoming;
        try {
            connection = new Socket(hostName, LISTENING_PORT);
            incoming = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            while (true) {
                String recContent = incoming.readLine();
                if (recContent == null)
                    break;
                else {
                    System.out.println(recContent);
                }
            }
            incoming.close();

        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
    }
}