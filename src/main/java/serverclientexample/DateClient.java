package serverclientexample;

import java.io.*;
import java.net.Socket;

public class DateClient {

    private static final int LISTENING_PORT = 1732;

    public static void main(String[] args) {
        String hostName = "127.0.0.1";

        try (Socket connection = new Socket(hostName, LISTENING_PORT);
             BufferedReader incoming = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
            String lineFromServer = incoming.readLine();
            if (lineFromServer == null) {
                throw new IOException("Connection was opened, but server did not send any data.");
            }
            System.out.printf("%n%s%n%n", lineFromServer);
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
    }

}
