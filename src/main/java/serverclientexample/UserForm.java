package serverclientexample;

import javax.swing.*;

public class UserForm {

    private static JButton btn;

    public static void main(String[] args) {
        JFrame frame = new JFrame("File Manager");

        btn = new JButton("Play");
        btn.setBounds(100, 100, 140, 40);
        frame.add(btn);

        frame.setSize(300, 400);
        frame.setLayout(null);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
