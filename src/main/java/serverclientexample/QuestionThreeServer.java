package serverclientexample;

import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public  class QuestionThreeServer {

    public static final int LISTENING_PORT = 32012;

    public static void main(String[] args) {
        String fileLocation;
        Scanner scanner;
        File directory;
        System.out.println("Please Enter files directory:");
        scanner = new Scanner(System.in);
        fileLocation = scanner.nextLine().trim();

        directory = new File(fileLocation);

        if (directory.isDirectory() == false)
            System.out.print("There is no such directory!");
        else {
            ServerSocket listener;
            Socket connection;

            try {
                listener = new ServerSocket(LISTENING_PORT);
                System.out.println("Listening on port... " + LISTENING_PORT);
                // while (true) {
                connection = listener.accept();
                sendFile(connection);
                listener.close();
                //}
            } catch (Exception e) {
                System.out.println("\"Sorry, the server has shut down.\"");
                System.out.println("Error: " + e);
                return;
            }
        }
    }

    public static void sendFile(Socket client) {
        PrintWriter outgoing;
        String fileContent = "";
        try {
            System.out.println("Connection from " + client.getInetAddress().toString());
            String fileName = "I:\\test\\B\\SubB.txt";
            File file = new File(fileName);
            FileReader fr = new FileReader(file);
            char[] chars = new char[(int) file.length()];
            fr.read(chars);
            fileContent = new String(chars);
            outgoing = new PrintWriter(client.getOutputStream());
            outgoing.print(fileContent);
            outgoing.flush();

            fr.close();
            client.close();
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }

    }
}

